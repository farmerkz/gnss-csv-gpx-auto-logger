/*============================================================================
// Program: GNSS GPX-CSV autologger
//
// Description: This program records a track using the GNSS module, which provides information 
// in the NMEA-0183 format. The track is saved to the SD card in GPX and/or UNICSV format.
// Tracks are saved in folders, whose names are formed according to the template YYYYMMDD,
// files are stored in the appropriate folders, the file name indicates the time in trHHMMSS
// format and .gpx  and .csv extensions respectively.
// The program can be compiled either for Arduino Nano or for Sparkfun Openlog, but Arduino 
// Nano is only recommended for debugging
// The track begins to be recorded only if there is a valid position. If the coordinates 
// are missing for more than 10 minutes, the current file is closed, and the next time a 
// valid position is found, a new file vith new name is opened.
//
//  Prerequisites:
//    - GNSS device has been correctly powered.
//    - GNSS device is correctly connected to an Arduino serial port.
//    - GNSS device has been configured for 3d Fix.
//    - You know the baud rate of your GPS device. 38400 is recomended and used in this programm
//    - Correctly configured library NeoGPS (see https://github.com/SlashDevin/NeoGPS for details)
//
// The program was developed in Visual Studio Code with PlatformIO extension
//
//  License:
//    Copyright (C) 2019, farmerkz
//
//    GNSS GPX-CSV autologger is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    GNSS GPX-CSV autologger is distributed WITHOUT ANY WARRANTY; 
//    without even the implied warranty of MERCHANTABILITY or FITNESS 
//    FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with GNSS GPX-CSV autologger.  If not, see <http://www.gnu.org/licenses/>.
//
//  Thanks:
//    1. SlashDevin (https://github.com/SlashDevin) - for a librarys NeoGPS, 
//       NeoHWSerial and NeoICSerial
//    2. greiman (https://github.com/greiman) - for a library SdFat-beta
//
//  Notes:
//    This is my first C++ program in general and for Arduino including :)
//============================================================================*/

//----------------------------------------------------------------
// Device type. Define only one.
//----------------------------------------------------------------
#define OPENLOG // SparkFun OpenLog https://github.com/sparkfun/OpenLog
//#define NANO // Arduino Nano, used for debugging and testing

//----------------------------------------------------------------
// Log format. Use one of them or both
//----------------------------------------------------------------
#define CSV
#define GPX

//----------------------------------------------------------------
// For the GPX format, determine the tail of the GPX file and its length
//----------------------------------------------------------------
#ifdef GPX
#define GPX_EPILOGUE "</trkseg></trk>\n</gpx>\n"
#define SEEK_TRKPT_BACKWARDS -23
#endif

//----------------------------------------------------------------
// Libraries
//----------------------------------------------------------------
#include <Arduino.h>
#include "NMEAGPS.h" // NeoGPS library. For this program changes were made suitable for me
                     // NMEAGPS_cfg and GPSfix_cfg
                     /* NMEAGPS_cfg:
                        ======
                        #define NMEAGPS_PARSE_GGA
                        //#define NMEAGPS_PARSE_GLL
                        #define NMEAGPS_PARSE_GSA
                        #define NMEAGPS_PARSE_GSV
                        //#define NMEAGPS_PARSE_GST
                        #define NMEAGPS_PARSE_RMC
                        //#define NMEAGPS_PARSE_VTG
                        #define NMEAGPS_PARSE_ZDA
                        ======
                        #define LAST_SENTENCE_IN_INTERVAL NMEAGPS::NMEA_ZDA  // You must replace this as needed for your GNSS module.
                        ======
                        #define NMEAGPS_FIX_MAX 2
                        ======
                        #define NMEAGPS_INTERRUPT_PROCESSING
                      */
                     /* GPSFix_cfg.h
                      #define GPS_FIX_DATE
                      #define GPS_FIX_TIME
                      #define GPS_FIX_LOCATION
                      //#define GPS_FIX_LOCATION_DMS
                      #define GPS_FIX_ALTITUDE
                      #define GPS_FIX_SPEED
                      //#define GPS_FIX_VELNED
                      #define GPS_FIX_HEADING
                      #define GPS_FIX_SATELLITES
                      #define GPS_FIX_HDOP
                      #define GPS_FIX_VDOP
                      #define GPS_FIX_PDOP
                      //#define GPS_FIX_LAT_ERR
                      //#define GPS_FIX_LON_ERR
                      //#define GPS_FIX_ALT_ERR
                      //#define GPS_FIX_SPD_ERR
                      //#define GPS_FIX_HDG_ERR
                      //#define GPS_FIX_TIME_ERR
                      #define GPS_FIX_GEOID_HEIGHT
                      */
//----------------------------------------------------------------
// We describe the ports - each board has its own port configuration
//----------------------------------------------------------------
#ifdef OPENLOG
#include "NeoHWSerial.h" // We use the driver for the hardware port
#define gpsPort NeoSerial
#define GPS_PORT_NAME "NeoSerial"
#define GPS_PORT_SPEED 38400 // GNSS module port speed, by default I configure the GNSS port to 38400 baud
//DEBUG_PORT not used due to lack of it
#endif

// Адуино Nano - SoftWare port for GNSS, HardWare - port for debug
#ifdef NANO
#include "NeoICSerial.h" // We use the improved SoftWare driver
NeoICSerial gpsPort;     // Use pins 8 & 9 for Nano
#define GPS_PORT_NAME "NeoICSerial"
#define GPS_PORT_SPEED 38400 // GNSS module port speed, by default I configure the GNSS port to 38400 baud
#define DEBUG_PORT Serial
#define DEBUG_PORT_SPEED 115200 // Port speed for debugging, hardware port, using maximum speed
#endif

//----------------------------------------------------------------
// SD card - definitions
//----------------------------------------------------------------
#include <SPI.h>
#include "SdFat.h"    //See https://github.com/greiman/SdFat-beta for details
#define SD_FAT_TYPE 0 // Hardware SPI

//----------------------------------------------------------------
// For Nano chip select - much more familiar to me
// For OpenLog - 10 pin
//----------------------------------------------------------------
#if defined NANO
const uint8_t SD_CS_PIN = 4;
#endif

#if defined OPENLOG
const uint8_t SD_CS_PIN = 10;
#endif

//----------------------------------------------------------------
// For OpenLog and Nano MISO_PIN = 12; MOSI_PIN = 11; SCK_PIN  = 13
//----------------------------------------------------------------
#define SD_CONFIG SdSpiConfig(SD_CS_PIN)
SdFat32 SD;

//----------------------------------------------------------------
// Check configuration
//----------------------------------------------------------------
#if !defined(GPS_FIX_TIME) || !defined(GPS_FIX_LOCATION)
#error You must define TIME and LOCATION in GPSfix_cfg.h
#endif

#if !defined(NMEAGPS_PARSE_RMC)
#error You must define NMEAGPS_PARSE_RMC in NMEAGPS_cfg.h!
#endif

#ifndef NMEAGPS_INTERRUPT_PROCESSING
#error You must define NMEAGPS_INTERRUPT_PROCESSING in NMEAGPS_cfg.h!
#endif

#if !defined(OPENLOG) && !defined(NANO)
#error You must define one of devices
#endif

#if (defined(OPENLOG) && defined(NANO))
#error You must define only one device
#endif

#if !defined(CSV) && !defined(GPX)
#error You must define one or both of formates
#endif

//----------------------------------------------------------------
// Declare an instance of NMEAGPS
static NMEAGPS gps;
//----------------------------------------------------------------

//----------------------------------------------------------------
// Some global variables
//----------------------------------------------------------------
static char buf_dir[9];  // Date for folder name
static char buf_file[7]; // Time for file name
#ifdef GPX
static char GPX_EXT[] = ".gpx"; // GPX file name extension
#endif
#ifdef CSV
static char CSV_EXT[] = ".csv"; // File name extension for CSV format
#endif
static gps_fix fix; // Globally declare a fix object

//----------------------------------------------------------------
// Set these values to the offset of your timezone from GMT
//----------------------------------------------------------------
static const int32_t zone_hours = 6L; // Asia/Alm, not DST in our timezone
static const int32_t zone_minutes = 0L;
static const NeoGPS::clock_t zone_offset =
    zone_hours * NeoGPS::SECONDS_PER_HOUR +
    zone_minutes * NeoGPS::SECONDS_PER_MINUTE;

//----------------------------------------------------------------
// Function and Subroutine Declarations
//----------------------------------------------------------------
void GPSisr(uint8_t c);                      // Interrupt handler
void initSD();                               // SD card initialization
void waitForFix();                           // Waiting for valid location
void fileDataTime();                         // Date and time from GPS for the
                                             //name of the folder and file, the result in global arrays.
File32 openTimestampedFile(char *extention); // Create and open the folder and file for the track.
                                             //Parameter - extension, returns - File32 object
#ifdef CSV
void csvHead(File32 &csv_log);                            // Writing the header file in UNICSV format
void gpsPointCSV(File32 &file, uint16_t lastLoggingTime); // Writing a point to a CSV file
#endif
#ifdef GPX
void gpxHead(File32 &gpx_log);                            // Writing the header file in GPX format
void gpsPointGPX(File32 &file, uint16_t lastLoggingTime); // Writing a point to a GPS file
#endif
void printL();
void adjustTime(NeoGPS::time_t &dt); // Cast date and time to local

//
//----------------------------------------------------------------
//
void setup()
{
#ifdef DEBUG_PORT
  DEBUG_PORT.begin(DEBUG_PORT_SPEED);
  while (!DEBUG_PORT)
  {
    // wait for serial port to connect.
  }

  DEBUG_PORT.println(F("NMEASDlog.ino started!"));
  DEBUG_PORT.print(F("fix size = "));
  DEBUG_PORT.println(int(sizeof(gps_fix)));
  DEBUG_PORT.print(NMEAGPS_FIX_MAX);
  DEBUG_PORT.println(F(" GPS updates can be buffered."));

  if (gps.merging != NMEAGPS::EXPLICIT_MERGING)
    DEBUG_PORT.println(F("Warning: EXPLICIT_MERGING should be enabled for best results!"));
#endif
  // Activate the interrupt handler to receive information from the GPS module
  gpsPort.attachInterrupt(GPSisr);
  // Activate the GPS port
  gpsPort.begin(GPS_PORT_SPEED);
  // Initiate an SD card
  initSD();
}

void loop()
{
  bool flag;
  uint16_t fixTime;
  waitForFix();
  fileDataTime();

#ifdef DEBUG_PORT
  DEBUG_PORT.println(F("Enter to main loop"));
#endif

#ifdef CSV
  File32 csv_log = openTimestampedFile(CSV_EXT);
#endif

#ifdef GPX
  File32 gpx_log = openTimestampedFile(GPX_EXT);
#endif

#ifdef CSV
  csvHead(csv_log);
#endif

#ifdef GPX
  gpxHead(gpx_log);
#endif
  flag = true;
  fixTime = (uint16_t)millis();
  while (flag)
  {
    // We work in this cycle while valid locations are available.
    if (gps.available())
    {
      fix = gps.read();
      if (fix.valid.status && (fix.status >= gps_fix::STATUS_STD))
      {
        static uint16_t lastLoggingTime = 0;
        uint16_t startLoggingTime = millis();
#ifdef DEBUG_PORT
        DEBUG_PORT.print(fix.satellites);
        DEBUG_PORT.print(F(" | "));
#endif

#ifdef CSV
        gpsPointCSV(csv_log, lastLoggingTime);
#endif

#ifdef GPX
        gpsPointGPX(gpx_log, lastLoggingTime);
#endif
        lastLoggingTime = (uint16_t)millis() - startLoggingTime;
        fixTime = (uint16_t)millis();
      }
    }
    else
    {
      if ((uint16_t)millis() - fixTime > 600000) //No valid position for more than 10 minutes
      {
#ifdef CSV
        csv_log.close();
#endif

#ifdef GPX
        gpx_log.close();
#endif
        flag = false;
        fixTime = (uint16_t)millis();
      }
    }
  }
#ifdef DEBUG_PORT
  DEBUG_PORT.println(F("End main loop"));
#endif
}

//----------------------------------------------------------------
// Interrupt handler
//----------------------------------------------------------------
void GPSisr(uint8_t c)
{
  gps.handle(c);
} // GPSisr()

//----------------------------------------------------------------
// We open SD, if an error - we leave in an infinite loop
//----------------------------------------------------------------
void initSD()
{

#ifdef DEBUG_PORT
  DEBUG_PORT.println(F("Initializing SD card..."));
#endif
  // see if the card is present and can be initialized:
  if (!SD.begin(SD_CONFIG))
  {
#ifdef DEBUG_PORT
    DEBUG_PORT.println(F("  SD card failed, or not present"));
#endif

    while (true)
    {
    }
  }

#ifdef DEBUG_PORT
  DEBUG_PORT.println(F("  SD card initialized."));
#endif

} // initSD ()

//----------------------------------------------------------------
// GPS validation Pending
//----------------------------------------------------------------
void waitForFix()
{
#ifdef DEBUG_PORT
  DEBUG_PORT.print(F("Waiting for GPS fix..."));
#endif
  uint16_t lastToggle = millis();
  for (;;)
  {
    if (gps.available())
    {
      if (gps.read().valid.location)
        break; // Got it!
    }
    if ((uint16_t)millis() - lastToggle > 500)
    {
      lastToggle += 500;

#ifdef DEBUG_PORT
      DEBUG_PORT.write('+');
#endif
    }
  }

#ifdef DEBUG_PORT
  DEBUG_PORT.println();
  DEBUG_PORT.println(F("GPS have valid location"));
#endif

  gps.overrun(false); // we had to wait a while...
} // waitForFix()

//----------------------------------------------------------------
//  We are waiting for a valid fix and take the date and time from there, 
//    fill them with global arrays buf_dir and buf_file
//----------------------------------------------------------------
void fileDataTime()
{
#ifdef DEBUG_PORT
  DEBUG_PORT.print(F("Is GPS available? "));
#endif
  uint16_t lastToggle = millis();
  while (!gps.available())
  {
    if ((uint16_t)millis() - lastToggle > 500)
    {
      lastToggle += 500;
#ifdef DEBUG_PORT
      DEBUG_PORT.print(F("-"));
#endif
    }
  }
#ifdef DEBUG_PORT
  DEBUG_PORT.println();
  DEBUG_PORT.println(F("Yes!!! GPS available"));
#endif

  fix = gps.read();
#ifdef DEBUG_PORT
  DEBUG_PORT.print(F("Is GPS have fix status STD? "));
#endif
  while (!(fix.valid.status && (fix.status >= gps_fix::STATUS_STD)))
  {
#ifdef DEBUG_PORT
    DEBUG_PORT.print(F("-"));
#endif
  }
#ifdef DEBUG_PORT
  DEBUG_PORT.println();
  DEBUG_PORT.println(F("Yes!!! GPS fix status is STD"));
#endif
  adjustTime(fix.dateTime);
  sprintf(
      buf_dir,
      "%s%02d%02d%02d",
      "20",
      fix.dateTime.year,
      fix.dateTime.month,
      fix.dateTime.date);
  sprintf(
      buf_file,
      "%02d%02d%02d",
      fix.dateTime.hours,
      fix.dateTime.minutes,
      fix.dateTime.seconds);
}

//----------------------------------------------------------------
// We take the date from buf_dir, from buf_file, create the file in the folder,
// return File32 object. In case error we go into an endless loop
//----------------------------------------------------------------
File32 openTimestampedFile(char *extention)
{
  char filename[13];
  File32 file;

#ifdef DEBUG_PORT
  DEBUG_PORT.print(F("Starting directory "));
  DEBUG_PORT.println(buf_dir);
#endif

  if (!SD.chdir())
  {
#ifdef DEBUG_PORT
    DEBUG_PORT.println(F("Error cd / "));
#endif
    while (true)
    {

    }
  }

  if (!SD.exists(buf_dir))
  {
    if (!SD.mkdir(buf_dir))
    {
#ifdef DEBUG_PORT
      DEBUG_PORT.print(F("Error creating directory "));
      DEBUG_PORT.println(buf_dir);
#endif
      while (true)
      {

      }
    }
  }

  sprintf(filename,
          "tr%s%s",
          buf_file,
          extention);

#ifdef DEBUG_PORT
  DEBUG_PORT.print(F("Starting file "));
  DEBUG_PORT.println(filename);
#endif

  SD.chdir(buf_dir);
  if (SD.exists(filename))
  {
#ifdef DEBUG_PORT
    DEBUG_PORT.println(F("warning: already exists, overwriting."));
#endif
  }
  //  if (!file.open(filename, (0x02 | 0x10 | 0x08 )))
  if (!file.open(filename, FILE_WRITE))
  {
#ifdef DEBUG_PORT
    DEBUG_PORT.print(F("Error creating file "));
    DEBUG_PORT.println(filename);
#endif
    while (true)
    {
      /* code */
    }
  }
  return file;
} // openTimestampedFile()

#ifdef CSV
//----------------------------------------------------------------
// We write the header in UNICSV format to the file
//----------------------------------------------------------------
void csvHead(File32 &file)
{
#ifdef DEBUG_PORT
  DEBUG_PORT.println(F("Enter to csvHead"));
#endif
  file.println(F("lat,long,alt,speed,head,date,time,sat,hdop,vdop,pdop,fix,notes"));
  file.flush();
#ifdef DEBUG_PORT
  DEBUG_PORT.println(F("Exit from csvHead"));
#endif
} //csvHead
#endif

#ifdef GPX
//----------------------------------------------------------------
// We write the header in GPS format to the file
//----------------------------------------------------------------
void gpxHead(File32 &file)
{
#ifdef DEBUG_PORT
  DEBUG_PORT.println(F("Enter to gpsHead"));
#endif
  file.print(F(
      "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
      "<gpx version=\"1.0\" creator=\"My GPS logger\" xmlns=\"http://www.topografix.com/GPX/1/0\">\n"));
  file.print(F("<name>GPS-"));
  for (int i = 0; i <= 7; i++)
  {
    file.print(buf_dir[i]);
  }
  file.print(F("</name>\n"));
  file.print(F("<trk>"));
  file.print(F("<name>TRK-"));
  for (int i = 0; i <= 7; i++)
  {
    file.print(buf_dir[i]);
  }
  file.print(F("-"));
  for (int i = 0; i <= 5; i++)
  {
    file.print(buf_file[i]);
  }
  file.print(F("</name>"));
  file.print(F("<trkseg>\n"));
  file.print(F(GPX_EPILOGUE));
  file.flush();
#ifdef DEBUG_PORT
  DEBUG_PORT.println(F("Exit from gpsHead"));
#endif
} //gpxHead
#endif

//----------------------------------------------------------------
//  Utility to print a long integer like it's a float
//    with 9 significant digits.
//----------------------------------------------------------------
void printL(Print &outs, int32_t degE7)
{
  // Extract and print negative sign
  if (degE7 < 0)
  {
    degE7 = -degE7;
    outs.print('-');
  }

  // Whole degrees
  int32_t deg = degE7 / 10000000L;
  outs.print(deg);
  outs.print('.');

  // Get fractional degrees
  degE7 -= deg * 10000000L;

  // Print leading zeroes, if needed
  if (degE7 < 10L)
    outs.print(F("000000"));
  else if (degE7 < 100L)
    outs.print(F("00000"));
  else if (degE7 < 1000L)
    outs.print(F("0000"));
  else if (degE7 < 10000L)
    outs.print(F("000"));
  else if (degE7 < 100000L)
    outs.print(F("00"));
  else if (degE7 < 1000000L)
    outs.print(F("0"));

  // Print fractional degrees
  outs.print(degE7);
} // printL()

#ifdef CSV
//----------------------------------------------------------------
// We write a point to the file in CSV format
//----------------------------------------------------------------
void gpsPointCSV(File32 &file, uint16_t lastLoggingTime)
{
  printL(file, fix.latitudeL());
  file.print(F(","));
  printL(file, fix.longitudeL());
  file.print(F(","));
  file.print(fix.altitude());
  file.print(F(","));
  file.print(fix.speed_kph());
  file.print(F("kmh"));
  file.print(F(","));
  file.print(fix.heading());
  file.print(F(","));
  file.print(F("20"));
  file.print(fix.dateTime.year);
  file.print(F("/"));
  file.print(fix.dateTime.month);
  file.print(F("/"));
  file.print(fix.dateTime.date);
  file.print(F(","));
  if (fix.dateTime.hours < 10)
    file.print(F("0"));
  file.print(fix.dateTime.hours);
  file.print(F(":"));
  if (fix.dateTime.minutes < 10)
    file.print(F("0"));
  file.print(fix.dateTime.minutes);
  file.print(F(":"));
  if (fix.dateTime.seconds < 10)
    file.print(F("0"));
  file.print(fix.dateTime.seconds);
  file.print(F("."));
  if (fix.dateTime_cs < 10)
    file.print(F("0"));
  file.print(fix.dateTime_cs);
  file.print(F(","));
  file.print(fix.satellites);
  file.print(F(","));
  file.print(fix.hdop * 0.001);
  file.print(F(","));
  file.print(fix.vdop * 0.001);
  file.print(F(","));
  file.print(fix.pdop * 0.001);
  file.print(F(","));
  file.print(F("3d"));
  file.print(F(","));
  file.print(lastLoggingTime);
  file.println();
  file.flush();
} // gpsPointCSV
#endif

#ifdef GPX
//----------------------------------------------------------------
// We write a point to the file in GPX format
//----------------------------------------------------------------
void gpsPointGPX(File32 &file, uint16_t lastLoggingTime)
{
  file.seekSet(file.fileSize() + SEEK_TRKPT_BACKWARDS);
  file.print(F("<trkpt "));
  file.print(F("lat=\""));
  printL(file, fix.latitudeL());
  file.print(F("\" lon=\""));
  printL(file, fix.longitudeL());
  file.print(F("\" >"));
  file.print(F("<time>"));
  file.print(F("20"));
  file.print(fix.dateTime.year);
  file.print(F("-"));
  file.print(fix.dateTime.month);
  file.print(F("-"));
  file.print(fix.dateTime.date);
  file.print(F("T"));
  if (fix.dateTime.hours < 10)
    file.print(F("0"));
  file.print(fix.dateTime.hours);
  file.print(F(":"));
  if (fix.dateTime.minutes < 10)
    file.print(F("0"));
  file.print(fix.dateTime.minutes);
  file.print(F(":"));
  if (fix.dateTime.seconds < 10)
    file.print(F("0"));
  file.print(fix.dateTime.seconds);
  file.print(F("."));
  if (fix.dateTime_cs < 10)
    file.print(F("0"));
  file.print(fix.dateTime_cs);
  file.print(F("Z"));
  file.print(F("</time>"));
  file.print(F("<ele>"));
  file.print(fix.altitude());
  file.print(F("</ele>"));
  file.print(F("<speed>"));
  file.print(fix.speed_kph() * 0.2777778);
  file.print(F("</speed>"));
  file.print(F("<course>"));
  file.print(fix.heading());
  file.print(F("</course>"));
  file.print(F("<sat>"));
  file.print(fix.satellites);
  file.print(F("</sat>"));
  file.print(F("<hdop>"));
  file.print(fix.hdop * 0.001);
  file.print(F("</hdop>"));
  file.print(F("<vdop>"));
  file.print(fix.vdop * 0.001);
  file.print(F("</vdop>"));
  file.print(F("<pdop>"));
  file.print(fix.pdop * 0.001);
  file.print(F("</pdop>"));
  file.print(F("<geoidheight>"));
  file.print(fix.geoidHeight());
  file.print(F("</geoidheight>"));
  file.print(F("<fix>3d</fix>"));
  file.print(F("<cmt>"));
  file.print(lastLoggingTime);
  file.print(F("</cmt>"));
  file.print(F("</trkpt>\n"));
  file.print(F(GPX_EPILOGUE));
  file.flush();
} // gpsPointGPX
#endif

//----------------------------------------------------------------
// Cast date and time to local
//----------------------------------------------------------------
void adjustTime(NeoGPS::time_t &dt)
{
  NeoGPS::clock_t seconds = dt; // convert date/time structure to seconds
                                //  First, offset from UTC to the local timezone
  seconds += zone_offset;
  dt = seconds; // convert seconds back to a date/time structure
} // adjustTime
