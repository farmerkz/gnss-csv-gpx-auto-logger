# GNSS auto logger with GPX and CSV format

This program records a track using the GNSS module, which provides information in the NMEA-0183 format. The track is saved to the SD card in GPX and/or UNICSV format.

Tracks are saved in folders, whose names are formed according to the template YYYYMMDD, files are stored in the appropriate folders, the file name indicates the time in trHHMMSS format and .gpx  and .csv extensions respectively.

The program can be compiled either for Arduino Nano or for Sparkfun Openlog, but Arduino Nano is only recommended for debugging.

The track begins to be recorded only if there is a valid position. If the coordinates are missing for more than 10 minutes, the current file is closed, and the next time a valid position is found, a new file vith new name is opened.
